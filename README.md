# PNG Collector

## Installation:

```bash
pip install -r requirements.txt
```

## Usage:

```bash
python3 main.py [-h] -U URL -O OUTPUT_DIR [-u USERNAME] [-p PASSWORD]

# Example:
python3 main.py -U https://thalamus.am -O ./data
```
