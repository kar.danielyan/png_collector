#!/usr/bin/env python3

_description_ = "Script for collecting PNG images from given URL."
_author_ = "Karen Danielyan"
_email_ = "kar.danielyan@gmail.com"
_license_ = "GPL"
_version_ = "1.0.1"

import os
import re
import sys
from argparse import ArgumentParser
from urllib.parse import urlparse
import requests


def message_formatting(message: str, type='SKIP'):
    colors = {
        'INFO': '\033[94m' + 'INFO: ',
        'SKIP': '\033[94m' + 'INFO: ',
        'SUCCESS': '\033[92m' + 'SUCCESS: ',
        'WARNING': '\033[93m' + 'WARNING: ',
        'ERROR': '\033[91m' + 'ERROR: '
    }
    print(colors.get(f'{type}'), message, '\033[0m')


def parse_arguments():
    parser = ArgumentParser(description='Script for collecting PNG images from given URL.')
    parser.add_argument("-U", "--url", required=True, help="URL (required)")
    parser.add_argument("-O", "--output-dir", required=True, help="Output directory path (required)")
    parser.add_argument("-u", "--username", help="Username (optional)")
    parser.add_argument("-p", "--password", help="Password (optional)")
    return parser


def get_page(url, auth=None):
    try:
        response = requests.get(url, allow_redirects=True, auth=auth)
        if response.ok:
            message_formatting(f"Successfully got HTML from URL {url}.", 'INFO')
            return response.text

        response.raise_for_status()
    except Exception as e:
        message_formatting(f"Couldn't get page from URL {url}, further processing is impossible, original exception: {e}.", 'ERROR')
        sys.exit(1)


def save_images(html, output_dir, host, auth=None):
    failed_img_urls = []
    png_pattern = re.compile(r"<img[^>]* src=\"([^\"]*\.png)\"[^>]*>")
    for img_url in png_pattern.findall(html):
        try:
            if img_url.startswith("//"):
                img_url = "https:" + img_url

            if not img_url.startswith("http"):
                img_url = host + img_url

            response = requests.get(img_url, allow_redirects=True, auth=auth)
            if response.ok:
                message_formatting(f"Successfully got image from URL {img_url}.", 'INFO')

                image_name = img_url.rsplit("/", 1)[1]
                out_file_path = f"{output_dir}/{image_name}"

                with open(out_file_path, "wb") as f:
                    f.write(response.content)
                    message_formatting(f"Successfully saved image to {out_file_path}.", 'INFO')

            response.raise_for_status()
        except Exception as e:
            message_formatting(f"Couldn't get image from URL {img_url}, original exception: {e}.", 'ERROR')
            failed_img_urls.append(img_url)

    return failed_img_urls


def main():
    arg_parser = parse_arguments()
    args = arg_parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    basic_auth = (args.username, args.password) if args.username and args.password else None

    url_parse_result = urlparse(args.url)
    port = f":{url_parse_result.port}" if url_parse_result.port else ""
    host_url = f"{url_parse_result.scheme}://{url_parse_result.hostname}{port}"

    raw_html = get_page(args.url, basic_auth)
    failed_images = save_images(raw_html, args.output_dir, host_url, basic_auth)

    if failed_images:
        message_formatting(f"Script completed with errors, list of failed images: {failed_images}.", 'ERROR')
        sys.exit(1)

    message_formatting(f"Script completed successfully.", 'SUCCESS')


if __name__ == '__main__':
    main()

